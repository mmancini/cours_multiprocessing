import multiprocessing as mp
import math
import time

def partial_pi(izero, iend, nb):
    ppi = 0
    nb_inv = 1. / nb
    for ii in range(izero,iend):
        jj = (ii + 0.5) * nb_inv
        ppi += 1./(1. + jj**2)
    return ppi


def partial_pi_wrap(args):
    # * collects all the positional arguments in a tuple
    return partial_pi(*args)


if __name__ == '__main__':

    nb = 100000000 #int(input("number of bins : "))
    nproc = mp.cpu_count()//2


    start = time.time()


    div   = nb // nproc
    if nb % nproc != 0 : div += 1
    nb = nproc * div

    pool = mp.Pool(nproc)

    # using Apply does not work
    # lista = ((ii*div,(ii+1)*div)  for ii in range(nproc))
    # result = (pool.apply(partial_pi, args=(x1,x2,nb)) for x1,x2 in lista)

    # using imap_unordered
    lista = ((ii*div,(ii+1)*div,nb)  for ii in range(nproc))
    result = pool.imap_unordered(partial_pi_wrap,lista)


    ppi = 4./nb * sum(result)
    end = time.time()

    print("Valeur approchee de pi : {} (delta={:e})".format(ppi,abs(ppi-math.pi)))
    print("Calculee en {} par {} processus".format(end-start,nproc))
