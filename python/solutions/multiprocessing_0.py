"""multiprocessing_0.py"""
import multiprocessing as mp


def worker(num):
    """thread worker function"""
    my_p = mp.current_process()
    print('Worker:', num,my_p.pid, my_p._parent_pid, my_p.name)



if __name__ == '__main__':
    jobs = []
    for i in range(5):
        p = mp.Process(target=worker, args=(i,), name='proc-'+str(i) )
        jobs.append(p)
        p.start()

    for i in jobs: i.join()
