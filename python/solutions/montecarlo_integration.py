"""montecarlo_integration.py"""

try :
    import sympy
except:
    pass
import math
import random
import time
import multiprocessing  as mp



# find ymin-ymax
def find_yminymax(function, xbound, numSteps=1000000):
    ymin = function(xbound[0])
    ymax = ymin

    for i in range(numSteps):
        x = xbound[0] + (xbound[1] - xbound[0]) * float(i) / numSteps
        y = function(x)
        #print(x,y)
        if y < ymin: ymin = y
        if y > ymax: ymax = y

    return ymin,ymax



# Monte Carlo
def integral(function, xbound, numPoints=1000000):
    xmin, xmax = xbound
    ymin, ymax = find_yminymax(function, xbound, numPoints)
    rectArea = (xmax - xmin) * (ymax - ymin)
    ctr = 0
    for j in range(numPoints):
        x = xmin + (xmax - xmin) * random.random()
        y = ymin + (ymax - ymin) * random.random()
        if math.fabs(y) <= math.fabs(function(x)):
            if function(x) > 0 and y > 0 and y <= function(x):
                ctr += 1 # area over x-axis is positive
            if function(x) < 0 and y < 0 and y >= function(x):
                ctr -= 1 # area under x-axis is negative

    baseArea = ymin * (xmax - xmin)
    fnArea = rectArea * float(ctr) / numPoints
    return fnArea + baseArea

def integral_wrap(args):
    # * collects all the positional arguments in a tuple
    return integral(*args)


if __name__ == '__main__':

    # define any xmin-xmax interval here! (xmin < xmax)
    xmin = 0.0
    xmax = 1.0 #2.0 * math.pi #1.0
    # define any function here!
    def f(x):
        '''1/(1+x**2)'''
        #return x
        #return math.sin(x)
        return 1./(1.+x**2.)


    print()
    print('Test integration of the function : ')
    print('             {}'.format(f.__doc__))

    try:
        tho_area = sympy.integrate(f.__doc__, ('x',xmin, xmax))
        print('Exact integration = {}'.format(tho_area))
    except:
        pass

    for N in (100000, 1000000, 5000000):

        ########################
        # Sequential computation

        start = time.time()
        seq_area = integral(f, [xmin, xmax],N)
        seq_time = time.time() - start

        ######################
        # Parallel computation

        start = time.time()
        nproc = mp.cpu_count()//2


        # With Ordered output
        # lista = ((f,
        #           [(xmax-xmin)/nproc*ii+xmin,
        #            (xmax-xmin)/nproc*(ii+1)+xmin],
        #           N//nproc)
        #          for ii in range(nproc))
        # with mp.Pool(processes=nproc) as pool:
        #     results = pool.map(integral_wrap, lista)
        #par_area = sum(results)

        # Without Order in output
        lista = ((f,
                  [(xmax-xmin)/nproc*ii+xmin,
                   (xmax-xmin)/nproc*(ii+1)+xmin],
                  N//nproc)
                 for ii in range(nproc))
        p = mp.Pool(processes=nproc)
        par_area = 0
        for ii in p.imap_unordered(integral_wrap,lista):
            par_area += ii


        p.close()
        p.join()

        # Using Apply : some problem with python 3.5 or on my laptop
        # lista2 = ([(xmax-xmin)/nproc*ii+xmin,(xmax-xmin)/nproc*(ii+1)+xmin] for ii in range(nproc))
        # p = mp.Pool(processes=nproc)
        # results = (p.apply(integral, args=(f,x,N//nproc)) for x in lista2)
        # par_area = sum(results)

        par_time = time.time() - start

        print('---------------------------------')
        print('Result for N = {}'.format(N))
        print(" parallel   integration = {}".format(seq_area))
        print(" sequential integration = {}".format(par_area))
        print('Timing:')
        print(' sequential              = {}'.format(seq_time))
        print(' parallel with {:2} procs  = {}'.format(nproc,par_time))


    time.sleep(100)
