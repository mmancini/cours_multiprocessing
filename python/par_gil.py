from intensive import busy_sleep
from threading import Thread


N = 100000000
t1 = Thread(target=busy_sleep, args=(N//2, ))
t2 = Thread(target=busy_sleep, args=(N//2, ))
t1.start()
t2.start()
t1.join()
t2.join()
