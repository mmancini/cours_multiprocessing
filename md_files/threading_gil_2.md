### Quelques problème avec *threading*

Avec deux threads:
```python
"""python/par_gil.py"""
from intensive import busy_sleep
from threading import Thread
N = 100000000
t1 = Thread(target=busy_sleep, args=(N//2, ))
t2 = Thread(target=busy_sleep, args=(N//2, ))
t1.start()
t2.start()
t1.join()
t2.join()
```

on obtient: <!-- .element: class="fragment" data-fragment-index="1" -->

```bash
time python python/seq_gil.py  ==>  4.796s
time python python/par_gil.py  ==>  5.243s
```
<!-- .element: class="fragment" data-fragment-index="1" -->
