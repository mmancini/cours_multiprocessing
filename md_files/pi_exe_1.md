#### La classe <span style="color:red" >*Pool*</span> : Exercice (Calcul de Pi en parallel)
On considere la suivante code sequentiel :
```python
'''pi_base.py'''
import multiprocessing
import math
import time

nb = 1000000 #int(input("number of bins : "))
nproc = 1

start = time.time()

ppi = 0.
for ii in range(0,nb):
    jj = (ii + 0.5)/nb
    ppi += 1./(1. + jj**2)

ppi = 4./nb * ppi
end = time.time()

print("Valeur approchee de pi : {} (delta={:e})".format(ppi,abs(ppi-math.pi)))
print("Calculee en {} par {} processus".format(end-start,nproc))
```
