#### La classe <span style="color:red" >*Process*</span> : Exemple precedent

```python
"""python/par_gil_process.py"""
from intensive import busy_sleep
from multiprocessing import Process
N = 100000000
p1 = Process(target=busy_sleep, args=(N//2, ))
p2 = Process(target=busy_sleep, args=(N//2, ))
p1.start()
p2.start()
p1.join()
p2.join()
```


on obtient: <!-- .element: class="fragment" data-fragment-index="1" -->

```bash
time python python/seq_gil.py  ==>  4.796s
time python python/par_gil_process.py  ==> 2.586
```
<!-- .element: class="fragment" data-fragment-index="1" -->


https://docs.python.org/3/library/multiprocessing.html
