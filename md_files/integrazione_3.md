#### La classe <span style="color:red" >*Pool*</span> : Exemple (integration Montecarlo)

Exercice : Parallélisation de l'intégration avec *Pool*


- Comment partager le calcul ?
- `Pool.map` ne gére pas bien les fonction de plusieurs arguments :
  ```python
  def integral_wrap(args):
    # * collects all the positional arguments in a tuple
    return integral(*args)

  ```

#### Solutions <!-- .element: class="fragment" data-fragment-index="1" -->
```python
# Without Order in output
lista = ((f,[(xmax-xmin)/nproc*ii+xmin,
            (xmax-xmin)/nproc*(ii+1)+xmin], N//nproc) for ii in range(nproc))
p = mp.Pool(processes=nproc)
par_area = 0
for part in p.imap_unordered(integral_wrap,lista):  par_area += part
```
<!-- .element: class="fragment" data-fragment-index="1" -->
```python
# Using Apply : some problem with python 3.5 or on my laptop
lista2 = ([(xmax-xmin)/nproc*ii+xmin,
           (xmax-xmin)/nproc*(ii+1)+xmin] for ii in range(nproc))
p = mp.Pool(processes=nproc)
results = (p.apply(integral, args=(f,x,N//nproc)) for x in lista2)
par_area = sum(results)
```
<!-- .element: class="fragment" data-fragment-index="1" -->
