#### La classe <span style="color:red" >*Pool*</span> : Exemple (integration Montecarlo)

Où la fonction pour calculer le max et le min de la coordonnée `y` est :

```python
# find ymin-ymax
def find_yminymax(function, xbound, numSteps=1000000):
    ymin = function(xbound[0])
    ymax = ymin

    for i in range(numSteps):
        x = xbound[0] + (xbound[1] - xbound[0]) * float(i) / numSteps
        y = function(x)
        #print(x,y)
        if y < ymin: ymin = y
        if y > ymax: ymax = y

    return ymin,ymax

```

Et pour lancer le calcul:
  <!-- .element: class="fragment" data-fragment-index="1" -->
```python
seq_area = integral(f, [xmin, xmax],N)
```
<!-- .element: class="fragment" data-fragment-index="1" -->
